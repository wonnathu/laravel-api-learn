<?php

Route::post('/register', 'AuthApi@register');
Route::post('/login', 'AuthApi@login');


Route::middleware(['auth:api'])->group(function () {
     Route::get('/feed', "FeedApi@index");
     Route::post('/feed/store', "FeedApi@store");
     Route::get('/feed/{feed_id}/edit', "FeedApi@edit");
     Route::get('/feed/{feed_id}/show', "FeedApi@show");
     Route::patch('/feed/{feed_id}' , "FeedApi@update");
     Route::delete('/feed/{feed_id}', "FeedApi@delete");
});