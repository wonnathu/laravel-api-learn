<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\User;
use App\Feed;
use App\Comment;
use App\Like;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        // $this->call(UsersTableSeeder::class);

        $user1 = new User();
        $user1->name = "WN2";
        $user1->email = 'wn2@gmail.com';
        $user1->password = bcrypt('password-123');
        $user1->save();

        $user1 = new User();
        $user1->name = "MS";
        $user1->email = 'ms@gmail.com';
        $user1->password = bcrypt('password-123');
        $user1->save();

        //Feeds
        for($i=0; $i<5; $i++){
            $feed = new Feed();
            $feed->user_id = rand(1,2);
            $feed->description = $faker->paragraph;
            $feed->image = "feed/default.jpg";
            $feed->save();
        }

        //Comments
        for($i=0; $i<5; $i++){
            $comment = new Comment();
            $comment->feed_id = rand(1,5);
            $comment->user_id = rand(1,2);
            $comment->comment = $faker->sentence;
            $comment->save();
        }

        //Likes
        
        $like = new Like();
        $like->user_id = 1;
        $like->feed_id = 1;
        $like->save();

        $like = new Like();
        $like->user_id = 2;
        $like->feed_id = 1;
        $like->save();
        
    }
}
