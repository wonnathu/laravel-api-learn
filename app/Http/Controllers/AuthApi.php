<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class AuthApi extends Controller
{
    public function register()
    {
        $validate = Validator::make(request()->all(),[
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'image' => 'mimes:jpg,jpeg,png,gif',
        ]);

        if($validate->fails()){
            return response()->json([
                'status' => 500,
                'message' => 'faded',
                'data' => $validate->errors(),
            ]);
        }

        $name = request()->name;
        $email = request()->email;
        $password = request()->password;
        $image = request()->image;        

        $image_name = uniqid().$image->getClientOriginalName();
        $image->move('/images', $image_name);

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->save();

        $token = $user->createToken('social')->accessToken;

        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $user,
            'token'=>$token,
        ]);

    }

    public function login()
    {
        $validate = Validator::make(request()->all(),[
            'email' => 'required',
            'password' => 'required'
        ]);

        if($validate->fails()){
            return response()->json([
                'status' => 500,
                'message' => 'login faded',
                'data' => $validate->errors(),
            ]);
        }

        $req_user = ['email' => request()->email, 'password' => request()->password];
        if(Auth::attempt($req_user)){
            $user = Auth::user();
            $token = $user->createToken('social')->accessToken;
            return response()->json([
                'status' => 200,
                'message' => 'login success',
                'data' => $user,
                'token' => $token,
            ]);
        }
        return response()->json([
            'status' => 500,
            'message' => 'login faded',
            'data' => [
                'error' => 'Email and password do not match',
            ]
        ]);
    }
}
