<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Feed;

class FeedApi extends Controller
{

    public function index()
    {
        $feeds = Feed::all();
        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $feeds
        ]);
    }

    public function store()
    {
        return request()->image;
        $validator = Validator::make(request()->all(),[
            'description' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png,gif'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => 'Failed',
                'data' => $validator->errors(),
            ]);
        }

        $image = request()->image;
        $image_name =uniqid() . $image->getClientOriginalName();
        $img_path = "feed/";
        $image->move($img_path, $image_name);

        $feed = new Feed();
        $feed->description = request()->description;
        $feed->user_id = Auth::id();
        $feed->image = $img_path.$image_name;
        $feed->save();

        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $feed
        ]);

    }

    public function edit($feed_id)
    {
        $feed = Feed::find($feed_id);
        if(!$feed){
            return response()->json([
                'status' => 404,
                'message' => 'failed',
                'data' =>[
                    'error'=> "Your Feed is not found"
                ]
            ]);
        }
        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' =>$feed,
        ]);
    }

    public function show($feed_id)
    {
        $feed = Feed::findOrFail($feed_id);

        if(!$feed){
            return response()->json([
                'status' => 404,
                'message' => 'failed',
                'data' => [
                    'error' => 'your Feed is not found'
                ]
            ]);
        }

        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $feed,
        ]);
    }

    public function update($feed_id)
    {

        $feed = Feed::find($feed_id);

        if(!$feed){
            return response()->json([
                'status' => 404,
                'message' => 'failed',
                'data' => [
                    'error' => 'Your feed is not found',
                ]
            ]);
        }

        $validator = Validator::make(request()->all(),[
            'description' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png,gif'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 500,
                'message' => 'failed',
                'data' => $validator->errors(),
            ]);
        }


        $image = request()->image;
        $image_name = uniqid(). $image->getClientOriginalName();
        $image_path = "feed/";

        $image->move($image_path, $image_name);
        
        $feed->description = request()->description;
        $feed->user_id = Auth::id();
        $feed->image = $image_path.$image_name;
        $feed->save();
        
        return response()->json([
            'status' => 200,
            'message' =>"success",
            'data' => $feed,
        ]);
    }

    public function delete($feed_id)
    {
        $feed = Feed::find($feed_id);
        if(!$feed){
            return response()->json([
                'status' => 404,
                'message' =>'failed',
                'data'=>[
                    'error' => 'Your Feed is not found',
                ]
            ]);
        }
        $feed->delete();
        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $feed,
        ]);
    }
}
